# WissVis Stream Line Seeds


## Project Setup

Follow the from [here](https://moodle2.uni-leipzig.de/pluginfile.php/2732683/mod_resource/content/6/VisPrak_22_23_Grundaufgaben.pdf) and then:


```bash
# it is assumed you are currently in the same directory as your build, src and prebuild directories

cd grundaufgabe_src
git clone git@git.informatik.uni-leipzig.de:ds76tabi/wissvis-stream-line-seeds.git

# rerun cmake
cd ../grundaufgabe_build
cmake ../grundaufgabe_src .
make
```

The plugin should now be loaded

## Project Structure

1. `algos` is the directory for FanToM plugins, so basically only for Files which run `AlgorithmRegister` at the end.
2. `include` is only for header files
3. `src` is for any other c++ code

## Libraries

The header-only lib for the Poisson Disk Distribution was copied from [here](https://github.com/thinks/poisson-disk-sampling)
The header-only lib for the Voronoi-Diagramms was copied from [here](https://www.chofter.com/mapviewer/voronoi.php) 
The [Eigen](https://eigen.tuxfamily.org/) library is used, which is part of the [project setup](#project-setup).
