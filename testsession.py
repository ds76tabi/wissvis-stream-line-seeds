### FAnToM Session
### API Version: 20170511
### Used core version:    e1e36b16181506379a22e4b161dbe60a26dd35af (VisPraktikum2021)
### Used toolbox version: fa980e33e3fbc76c9837a4ab19ea1a29a3c21f7f (devel)

################################################################
###                  Reset GUI                               ###
################################################################
fantom.ui.setCamera( 0, fantom.ui.Camera( fantom.math.Vector3(2.40655, -2.02674, 2.17002), fantom.math.Vector3(2.40655, -2.02674, 0.00158954), fantom.math.Vector3(0, 1, 0), 1, 1.0472 ) )
fantom.ui.setCamera( 1, fantom.ui.Camera( fantom.math.Vector3(3.66137, 0, 0), fantom.math.Vector3(0, 0, 0), fantom.math.Vector3(0, 0, 1), 0, 1.0472 ) )
fantom.ui.setCamera( 2, fantom.ui.Camera( fantom.math.Vector3(0, -3.66137, 0), fantom.math.Vector3(0, -3.8147e-06, 0), fantom.math.Vector3(0, 0, 1), 0, 1.0472 ) )
fantom.ui.setCamera( 3, fantom.ui.Camera( fantom.math.Vector3(0, 0, 3.66137), fantom.math.Vector3(0, 0, 0), fantom.math.Vector3(0, 1, 0), 0, 1.0472 ) )

fantom.ui.setClippingPlane( fantom.ui.ClippingPlane( 0, fantom.math.Vector4( 1, 0, 0, 1 ), False ) )
fantom.ui.setClippingPlane( fantom.ui.ClippingPlane( 1, fantom.math.Vector4( -1, 0, 0, 1 ), False ) )
fantom.ui.setClippingPlane( fantom.ui.ClippingPlane( 2, fantom.math.Vector4( 0, 1, 0, 1 ), False ) )
fantom.ui.setClippingPlane( fantom.ui.ClippingPlane( 3, fantom.math.Vector4( 0, -1, 0, 1 ), False ) )
fantom.ui.setClippingPlane( fantom.ui.ClippingPlane( 4, fantom.math.Vector4( 0, 0, 1, 1 ), False ) )
fantom.ui.setClippingPlane( fantom.ui.ClippingPlane( 5, fantom.math.Vector4( -0, 0, -1, 1 ), False ) )

fantom.ui.setBackgroundColor( fantom.math.Color(0, 0, 0, 1) )

fantom.ui.setRotationCenter( fantom.ui.RotationCenter( fantom.math.Vector3(0, 0, 0), True, True, True ) )


################################################################
###                  Create algorithms                       ###
################################################################
Load_VTK = fantom.makeAlgorithm("Load/VTK")
Load_VTK.setName("Load/VTK")
Load_VTK.setAutoSchedule(True)
Load_VTK.setOption("Input File", "/home/denis/workspace/uni/wissenschaftliche_visualisierung/hauptaufgabe/TestDataHauptaufgaben/generatedField.vtk")
Load_VTK.setOption("Big Endian", True)
Load_VTK.setOption("Dimension", "2D if third component is zero")
Load_VTK.setOption("Time List", "")
fantom.ui.setAlgorithmPosition(Load_VTK, fantom.math.Vector2(83, 47))

# Inbound connections of this algorithm:

# Run the algorithm
Load_VTK.runBlocking()

Hauptaufgabe_VermaStreamLines = fantom.makeAlgorithm("Hauptaufgabe/VermaStreamLines")
Hauptaufgabe_VermaStreamLines.setName("Hauptaufgabe/VermaStreamLines")
Hauptaufgabe_VermaStreamLines.setAutoSchedule(True)
Hauptaufgabe_VermaStreamLines.setOption("delta_seed", 0.2)
Hauptaufgabe_VermaStreamLines.setOption("delta_streamline", 0.07)
Hauptaufgabe_VermaStreamLines.setOption("seeding_distance_factor", 0.8)
Hauptaufgabe_VermaStreamLines.setOption("poisson_radius", 0.4)
Hauptaufgabe_VermaStreamLines.setOption("template_influence_ratio", 0.8)
Hauptaufgabe_VermaStreamLines.setOption("integration_steps", 1000)
Hauptaufgabe_VermaStreamLines.setOption("integration_step_size", 0.1)
fantom.ui.setAlgorithmPosition(Hauptaufgabe_VermaStreamLines, fantom.math.Vector2(40, 167))

# Inbound connections of this algorithm:
Load_VTK.connect("Fields", Hauptaufgabe_VermaStreamLines, "Field")

# Run the algorithm
Hauptaufgabe_VermaStreamLines.runBlocking()

PointSet_ShowPoints_3 = fantom.makeAlgorithm("Point Set/Show Points")
PointSet_ShowPoints_3.setName("Point Set/Show Points::3")
PointSet_ShowPoints_3.setAutoSchedule(True)
PointSet_ShowPoints_3.setOption("Scale", 0.02)
PointSet_ShowPoints_3.setOption("Color", fantom.math.Color(0.3843, 0.6275, 0.9176, 1))
PointSet_ShowPoints_3.setOption("Shading", "Blinn-Phong")
fantom.ui.setAlgorithmPosition(PointSet_ShowPoints_3, fantom.math.Vector2(-10, 591))
PointSet_ShowPoints_3.setVisualOutputVisible('Points', True)

# Inbound connections of this algorithm:
Hauptaufgabe_VermaStreamLines.connect("poissonPoints", PointSet_ShowPoints_3, "Points")

# Run the algorithm
PointSet_ShowPoints_3.runBlocking()

PointSet_ShowPoints_2 = fantom.makeAlgorithm("Point Set/Show Points")
PointSet_ShowPoints_2.setName("Point Set/Show Points::2")
PointSet_ShowPoints_2.setAutoSchedule(True)
PointSet_ShowPoints_2.setOption("Scale", 0.02)
PointSet_ShowPoints_2.setOption("Color", fantom.math.Color(0.898, 0.6471, 0.03922, 1))
PointSet_ShowPoints_2.setOption("Shading", "Blinn-Phong")
fantom.ui.setAlgorithmPosition(PointSet_ShowPoints_2, fantom.math.Vector2(-10, 419))
PointSet_ShowPoints_2.setVisualOutputVisible('Points', True)

# Inbound connections of this algorithm:
Hauptaufgabe_VermaStreamLines.connect("criticalPoints", PointSet_ShowPoints_2, "Points")

# Run the algorithm
PointSet_ShowPoints_2.runBlocking()

LineSet_ShowLineSet_2 = fantom.makeAlgorithm("Line Set/Show Line Set")
LineSet_ShowLineSet_2.setName("Line Set/Show Line Set::2")
LineSet_ShowLineSet_2.setAutoSchedule(True)
LineSet_ShowLineSet_2.setOption("Constant Radius", False)
LineSet_ShowLineSet_2.setOption("Visualisation Type", "lines")
LineSet_ShowLineSet_2.setOption("Illumination", False)
LineSet_ShowLineSet_2.setOption("Line width", 1)
LineSet_ShowLineSet_2.setOption("Line color", fantom.math.Color(0.5686, 0.2549, 0.6745, 1))
LineSet_ShowLineSet_2.setOption("Show nodes", False)
LineSet_ShowLineSet_2.setOption("Node size", 0.1)
LineSet_ShowLineSet_2.setOption("Node color", fantom.math.Color(1, 0.3, 0.3, 1))
LineSet_ShowLineSet_2.setOption("use ColorMap", False)
LineSet_ShowLineSet_2.setOption("Color Map", "7 0  0 0 0.2118 1  0.1667  0 0 1 1  0.3333  0 0.4902 1 1  0.5  0.05098 0.9961 1 1  0.6667  0.01961 0.4 0.1686 1  0.8333  0.902 1 0.7765 1  1  0.4 0.1686 0 1  2 0 1 1 1 ")
LineSet_ShowLineSet_2.setOption("set ColorMap range automatically", True)
LineSet_ShowLineSet_2.setOption("minValue", 0)
LineSet_ShowLineSet_2.setOption("maxValue", 1)
fantom.ui.setAlgorithmPosition(LineSet_ShowLineSet_2, fantom.math.Vector2(-10, 333))
LineSet_ShowLineSet_2.setVisualOutputVisible('LineSet', True)

# Inbound connections of this algorithm:
Hauptaufgabe_VermaStreamLines.connect("voronoiLines", LineSet_ShowLineSet_2, "Lineset")

# Run the algorithm
LineSet_ShowLineSet_2.runBlocking()

LineSet_ShowLineSet = fantom.makeAlgorithm("Line Set/Show Line Set")
LineSet_ShowLineSet.setName("Line Set/Show Line Set")
LineSet_ShowLineSet.setAutoSchedule(True)
LineSet_ShowLineSet.setOption("Constant Radius", False)
LineSet_ShowLineSet.setOption("Visualisation Type", "lines")
LineSet_ShowLineSet.setOption("Illumination", False)
LineSet_ShowLineSet.setOption("Line width", 1)
LineSet_ShowLineSet.setOption("Line color", fantom.math.Color(0.9, 0.9, 0.9, 1))
LineSet_ShowLineSet.setOption("Show nodes", False)
LineSet_ShowLineSet.setOption("Node size", 0.1)
LineSet_ShowLineSet.setOption("Node color", fantom.math.Color(1, 0.3, 0.3, 1))
LineSet_ShowLineSet.setOption("use ColorMap", False)
LineSet_ShowLineSet.setOption("Color Map", "7 0  0 0 0.2118 1  0.1667  0 0 1 1  0.3333  0 0.4902 1 1  0.5  0.05098 0.9961 1 1  0.6667  0.01961 0.4 0.1686 1  0.8333  0.902 1 0.7765 1  1  0.4 0.1686 0 1  2 0 1 1 1 ")
LineSet_ShowLineSet.setOption("set ColorMap range automatically", True)
LineSet_ShowLineSet.setOption("minValue", 0)
LineSet_ShowLineSet.setOption("maxValue", 1)
fantom.ui.setAlgorithmPosition(LineSet_ShowLineSet, fantom.math.Vector2(167, 267))
LineSet_ShowLineSet.setVisualOutputVisible('LineSet', True)

# Inbound connections of this algorithm:
Hauptaufgabe_VermaStreamLines.connect("streamLines", LineSet_ShowLineSet, "Lineset")

# Run the algorithm
LineSet_ShowLineSet.runBlocking()

PointSet_ShowPoints = fantom.makeAlgorithm("Point Set/Show Points")
PointSet_ShowPoints.setName("Point Set/Show Points")
PointSet_ShowPoints.setAutoSchedule(True)
PointSet_ShowPoints.setOption("Scale", 0.02)
PointSet_ShowPoints.setOption("Color", fantom.math.Color(0.6471, 0.1137, 0.1765, 1))
PointSet_ShowPoints.setOption("Shading", "Blinn-Phong")
fantom.ui.setAlgorithmPosition(PointSet_ShowPoints, fantom.math.Vector2(-10, 505))
PointSet_ShowPoints.setVisualOutputVisible('Points', True)

# Inbound connections of this algorithm:
Hauptaufgabe_VermaStreamLines.connect("templatePoints", PointSet_ShowPoints, "Points")

# Run the algorithm
PointSet_ShowPoints.runBlocking()



