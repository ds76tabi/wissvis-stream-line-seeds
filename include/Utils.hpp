#ifndef UTILS_H_
#define UTILS_H_


#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Datastructures.hpp>

namespace utils
{
    std::string vector_to_string(Vector2 v);
    double dot_product(Vector2 v, Vector2 u);
    Point2 get_right_angle_point_on_line(Point2 *point, datastructs::Line * line);
    double get_point_distance(Point2 p1, Point2 p2);
    Point2 get_middle_point(datastructs::Line line);
    Point2 get_closest_voronoi_line_point(datastructs::CriticalPoint *critical_point);
    double get_furthest_point_distance(datastructs::CriticalPoint *cp, std::vector<Point2> *points);
    std::vector<Point2> create_points_on_circle(Point2 center, double radius, Point2 startpoint, double delta_seed);
    std::vector<datastructs::Eigenvector> get_principal_eigenvectors(std::vector<datastructs::Eigenvector> principals);
    double angle_between_vectors(Vector2 a, Vector2 b);
    std::vector<datastructs::Eigenvector> order_vectors_by_angle(std::vector<datastructs::Eigenvector> principals);
    std::vector<Vector2> get_bisectors(std::vector<datastructs::Eigenvector> principals);
    Vector2 get_vector_between_points(Point2 source, Point2 target);
    void sort_eigenvectors_by_eigenvalue(std::vector<datastructs::Eigenvector> * eigenvectors);
}

#endif