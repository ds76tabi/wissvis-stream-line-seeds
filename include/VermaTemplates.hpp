#ifndef VERMATEMPLATES_H_
#define VERMATEMPLATES_H_

#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Datastructures.hpp>
#include <Utils.hpp>

#include "poisson_disk_sampling.h"


namespace verma
{
    class VermaTemplateGenerator
    {

    private:
        std::vector<Point2> create_points_on_circle(Point2 center, double radius, Point2 startpoint, double delta_seed);

        std::vector<Point2> get_saddle_template_points(datastructs::CriticalPoint *critical_point);

        std::vector<Point2> get_source_sink_template_points(datastructs::CriticalPoint *critical_point);

        std::vector<Point2> get_center_spiral_template_points(datastructs::CriticalPoint *critical_point);

    public:
        const double delta_seed;
        const double seeding_distance_factor;

        VermaTemplateGenerator(double delta, double seeding_distance_factor = 0.5) : delta_seed(delta), seeding_distance_factor(seeding_distance_factor) {};

        std::vector<Point2> get_template_points(datastructs::CriticalPoint *critical_point);
    };
};

#endif