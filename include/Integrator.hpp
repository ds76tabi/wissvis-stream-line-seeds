#ifndef INTEGRATOR_H_
#define INTEGRATOR_H_

#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Datastructures.hpp>

namespace integrator
{

    class Integrator
    {
    private:
        std::shared_ptr<Field<2, Tensor<double, 2>> const> field;
        std::shared_ptr<FieldEvaluator<2, Tensor<double, 2>>> evaluator;
        std::shared_ptr<LineSet<2>> line_set;
        double delta_streamlines;
        int integration_iterations;
        virtual bool get_next_value(Point<2> last, Point<2> &next) = 0;

    protected:
        bool check_termination(Point2 point);
        bool get_value(Point<2> input, Tensor<double, 2> &output);

    public:
        Integrator(std::shared_ptr<Field<2, Tensor<double, 2>> const> field, std::shared_ptr<LineSet<2>> line_set, double delta_streamlines, int integration_iterations) : field(field), evaluator(field->makeEvaluator()), line_set(line_set), delta_streamlines(delta_streamlines), integration_iterations(integration_iterations) {}
        void append_streamlines_for_seeds(std::vector<Point2> *seeds);
        virtual ~Integrator() {}
        std::vector<Point<2>> line_points(Point<2> start, int max_length);
    };

    class RungeKuttaIntegrator : public Integrator
    {
    private:
        Scalar const step_size;

    public:
        RungeKuttaIntegrator(std::shared_ptr<Field<2, Tensor<double, 2>> const> field, std::shared_ptr<LineSet<2>> line_set, double delta_streamlines, int integration_iterations, Scalar step_size) : Integrator(field, line_set, delta_streamlines, integration_iterations), step_size(step_size) {}
        virtual bool get_next_value(Point<2> last, Point<2> &next);
    };

    class AdaptiveEulerIntegrator : public Integrator
    {
    public:
        AdaptiveEulerIntegrator(
            std::shared_ptr<Field<2, Tensor<double, 2>> const> field,
            std::shared_ptr<LineSet<2>> line_set,
            double delta_streamlines,
            int integration_iterations,
            Scalar step_size,
            Scalar threshold_half,
            Scalar threshold_double) : Integrator(field, line_set, delta_streamlines, integration_iterations), step_size(step_size), threshold_half(threshold_half), threshold_double(threshold_double) {}

            bool get_next_value(Point<2> last,Point<2>& next);

    private:
        Scalar step_size;
        Scalar const threshold_half;
        Scalar const threshold_double;
    };
};

#endif