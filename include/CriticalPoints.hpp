// Description: determine voronoi tesselation for critical points
//  from a vector field
// Author: Isidor Zeuner
// iz32mega@studserv.uni-leipzig.de
// For license see: https://mit-license.org
// -------------------------------------------------------------------

#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>

#include <Eigen/Dense>

#include <boost/geometry.hpp>
#include <boost/polygon/voronoi.hpp>

#include <Datastructures.hpp>

using namespace fantom;

namespace
{
    typedef int voronoi_int;
    struct point {
        voronoi_int x;
        voronoi_int y;
        point(voronoi_int x, voronoi_int y) :
        x(x),
        y(y) {
        }
    };

    struct segment {
        point southwest;
        point northeast;
        segment(
            voronoi_int x1,
            voronoi_int y1,
            voronoi_int x2,
            voronoi_int y2
        ) :
        southwest(x1, y1),
        northeast(x2, y2) {
        }
    };
}

namespace boost {
    namespace polygon {
        template<>
        struct geometry_concept<point> {
            typedef point_concept type;
        };

        template<>
        struct point_traits<point> {
            typedef voronoi_int coordinate_type;
            static inline coordinate_type get(
                point const& point,
                orientation_2d oriented
            ) {
                return (oriented == HORIZONTAL) ? point.x : point.y;
            }
        };

        template<>
        struct geometry_concept<segment> {
            typedef segment_concept type;
        };

        template <>
        struct segment_traits<segment> {
            typedef int coordinate_type;
            typedef point point_type;
            static inline point_type get(segment const& seg, direction_1d dir) {
                return dir.to_int() ? seg.northeast : seg.southwest;
            }
        };
    }
}

namespace
{
    template<typename real>
    class discretization {
    public:
        discretization(real min, real max) :
        min_i(min),
        max_i(max),
        min_o(std::numeric_limits<voronoi_int>::min() / 2),
        max_o(std::numeric_limits<voronoi_int>::max() / 2) {
        }
        voronoi_int to_int(real from) const {
            return min_o + (from - min_i) * (max_o - min_o) / (max_i - min_i);
        }
        real to_real(real from) const {
            return min_i + (from - min_o) * (max_i - min_i) / (max_o - min_o);
        }
    private:
        real const min_i;
        real const max_i;
        real const min_o;
        real const max_o;
    };
    template<int D>
    struct point_embedding {
        static Point<3> embed_3d(Point<D> original) {
            Point<3> result;
            for (size_t i = 0; i < D; i++) {
                result(i) = original(i);
            }
            return result;
        }
        static Point<D> extract_3d(Point<3> embedded) {
            Point<D> result;
            for (size_t i = 0; i < D; i++) {
                result(i) = embedded(i);
            }
            return result;
        }
    };

    template<int D, typename value_type>
    class point_map {
    public:
        point_map(double epsilon = 0.00001) :
        epsilon(epsilon) {}
        bool insert(Point<D> key, value_type value) {
            for (
                auto checking = entries.begin();
                entries.end() != checking;
                checking++
            ) {
                if (norm(key - checking->first) < epsilon) {
                    return false;
                }
            }
            entries.push_back(std::make_pair(key, value));
            return true;
        }
        std::vector<
            std::pair<Point<D>, value_type>
        > as_iteratable() const {
            return entries;
        }
    private:
        double epsilon;
        std::vector<
            std::pair<Point<D>, value_type>
        > entries;
    };

    template<typename value_type>
    class point_map<2, value_type> {
        typedef boost::geometry::model::point<
            double,
            2,
            boost::geometry::cs::cartesian
        > point;
        typedef boost::geometry::model::box<point> box;
    public:
        point_map(double epsilon = 0.00001) :
        epsilon(epsilon) {}
        bool insert(Point<2> key, value_type value) {
            std::vector<
                std::pair<box, value_type>
            > candidates;
            entries.query(
                boost::geometry::index::intersects(
                    box(
                        point(key(0) - epsilon, key(1) - epsilon),
                        point(key(0) + epsilon, key(1) + epsilon)
                    )
                ),
                std::back_inserter(candidates)
            );
            for (
                auto checking = candidates.begin();
                candidates.end() != checking;
                checking++
            ) {
                point const found_boost = checking->first.min_corner();
                Point<2> const found(
                    found_boost.get<0>(),
                    found_boost.get<1>()
                );
                if (norm(key - found) < epsilon) {
                    return false;
                }
            }
            point const key_boost(key(0), key(1));
            entries.insert(
                std::make_pair(box(key_boost, key_boost), value)
            );
            return true;
        }
        std::vector<
            std::pair<Point<2>, value_type>
        > as_iteratable() const {
            std::vector<
                std::pair<Point<2>, value_type>
            > result;
            for (
                auto inserting = entries.begin();
                entries.end() != inserting;
                inserting++
            ) {
                point const key_boost = inserting->first.min_corner();
                Point<2> const key(key_boost.get<0>(), key_boost.get<1>());
                result.push_back(std::make_pair(key, inserting->second));
            }
            return result;
        }
    private:
        double epsilon;
        boost::geometry::index::rtree<
            std::pair<box, value_type>,
            boost::geometry::index::quadratic<16>
        > entries;
    };

    template<int D>
    class evaluator_eigen_functor_wrapper {
    public:
        typedef double Scalar;
        enum {
            InputsAtCompileTime = D,
            ValuesAtCompileTime = D,
        };
        typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
        typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
        typedef Eigen::Matrix<
            Scalar,
            ValuesAtCompileTime,
            InputsAtCompileTime
        > JacobianType;
        evaluator_eigen_functor_wrapper(
            std::shared_ptr<
                FieldEvaluator<
                    D,
                    Tensor<double, D>
                >
            > evaluator
        ) :
        evaluator(
            evaluator
        ) {
        }
        void operator()(InputType const& input, ValueType& output) const {
            Tensor<double, D> input_fantom;
            for (size_t i = 0; i < D; i++) {
                input_fantom(i) = input(i, 0);
            }
            if (!evaluator->reset(input_fantom)) {
                throw std::runtime_error("evaluator failed");
            }
            Tensor<double, D> const output_fantom = evaluator->value();
            for (size_t i = 0; i < D; i++) {
                output(i, 0) = output_fantom(i);
            }
        }
        size_t values() const {
            return InputsAtCompileTime;
        }
    private:
        std::shared_ptr<
            FieldEvaluator<
                D,
                Tensor<double, D>
            >
        > evaluator;
    };

    template<typename differentiator>
    class eigenvalue_evaluator {
    public:
        typedef typename differentiator::JacobianType JacobianType;
        eigenvalue_evaluator(differentiator& differentiating) :
        differentiating(differentiating) {
        }
        bool find_root(
            Point<differentiator::InputsAtCompileTime> const& input,
            Point<differentiator::InputsAtCompileTime>& root,
            double epsilon = 0.00000001,
            size_t max_steps = 64
        ) {
            typename differentiator::InputType input_eigen;
            for (size_t i = 0; i < differentiator::InputsAtCompileTime; i++) {
                input_eigen(i, 0) = input(i);
            }
            typename differentiator::ValueType value;
            JacobianType jacobian;
            for (size_t step = 0; step < max_steps; step++) {
                differentiating(input_eigen, value);
                if (value.norm() < epsilon) {
                    for (
                        size_t i = 0;
                        i < differentiator::InputsAtCompileTime;
                        i++
                    ) {
                        root(i) = input_eigen(i, 0);
                    }
                    return true;
                }
                differentiating.df(input_eigen, jacobian);
                typename differentiator::InputType delta(
                    jacobian.colPivHouseholderQr().solve(value)
                );
                input_eigen = input_eigen - delta;
            }
            return false;
        }
        bool find_critical_point(
            Point<differentiator::InputsAtCompileTime> const& input,
            Point<differentiator::InputsAtCompileTime>& root,
            enum datastructs::CriticalPointType& type,
            std::vector<datastructs::Eigenvector>& eigenvectors,
            double epsilon = 0.00000001,
            size_t max_steps = 64
        ) {
            if (
                !find_root(
                    input,
                    root,
                    epsilon,
                    max_steps
                )
            ) {
                return false;
            }
            JacobianType const jacobian = get_jacobian(
                root
            );
            Eigen::EigenSolver<JacobianType> solver(jacobian, true);
            auto const eigenvalues = solver.eigenvalues();
            auto const eigenvector_count = solver.eigenvectors().cols();
            eigenvectors.clear();
            bool only_real = true;
            bool only_zero_real = true;
            bool only_negative = true;
            bool only_positive = true;
            for (Eigen::Index i = 0; i < eigenvector_count; i++) {
                auto const eigenvalue = eigenvalues(i, 0);
                if (std::abs(eigenvalue.imag()) >= epsilon) {
                    only_real = false;
                }
                if (std::abs(eigenvalue.real()) < epsilon) {
                    only_positive = false;
                    only_negative = false;
                } else if (eigenvalue.real() < 0) {
                    only_positive = false;
                    only_zero_real = false;
                } else {
                    only_negative = false;
                    only_zero_real = false;
                }
                auto eigenvector_eigen = solver.eigenvectors().col(i);
                Point<differentiator::InputsAtCompileTime> eigenvector_fantom;
                for (
                    size_t j = 0;
                    j < differentiator::InputsAtCompileTime;
                    j++
                ) {
                    // only return real part of eigenvector as FAnToM vectors
                    //  are real-valued
                    eigenvector_fantom(j) = eigenvector_eigen(j, 0).real();
                }
                datastructs::Eigenvector eigenvector;
                eigenvector.vector = eigenvector_fantom;
                eigenvector.eigenvalue = eigenvalue;
                eigenvectors.push_back(eigenvector);
            }
            if (only_real) {
                if (only_positive) {
                    type = datastructs::SOURCE;
                    return true;
                } else if (only_negative) {
                    type = datastructs::SINK;
                    return true;
                } else {
                    type = datastructs::SADDLE;
                    return true;
                }
            } else {
                if (only_zero_real) {
                    type = datastructs::CENTER;
                    return true;
                } else if (only_positive) {
                    type = datastructs::SPIRAL; // spiral source
                    return true;
                } else if (only_negative) {
                    type = datastructs::SPIRAL; // spiral sink
                    return true;
                }
            }
            throw std::logic_error("unknown critical point classification");
        }
protected:
        JacobianType get_jacobian(
            Point<differentiator::InputsAtCompileTime> const& input
        ) {
            typename differentiator::InputType input_eigen;
            for (size_t i = 0; i < differentiator::InputsAtCompileTime; i++) {
                input_eigen(i, 0) = input(i);
            }
            JacobianType jacobian;
            differentiating.df(input_eigen, jacobian);
            return jacobian;
        }
    private:
        differentiator& differentiating;
    };

    template<typename point_type, typename linestring_type>
    class bounding_box {
    public:
        bounding_box(std::vector<point_type> corners) {
            for (auto corner: corners) {
                std::shared_ptr<point_type> wrapped(new point_type(corner));
                this->corners.push_back(wrapped);
            }
            auto last_corner = this->corners.back();
            std::shared_ptr<linestring_type> last_edge;
            for (auto current_corner: this->corners) {
                std::shared_ptr<linestring_type> current_edge(
                    new linestring_type
                );
                if (last_edge) {
                    order[last_edge.get()] = std::make_pair(
                        current_edge.get(),
                        last_corner.get()
                    );
                }
                current_edge->push_back(*last_corner);
                current_edge->push_back(*current_corner);
                edges.push_back(current_edge);
                last_corner = current_corner;
                last_edge = current_edge;
            }
            order[last_edge.get()] = std::make_pair(
                edges.front().get(),
                last_corner.get()
            );
        }
        template<typename intersectable_type>
        std::vector<
            std::pair<linestring_type const*, point_type>
        > intersection(intersectable_type intersectable) const {
            std::vector<
                std::pair<linestring_type const*, point_type>
            > result;
            for (auto edge: edges) {
                std::vector<point_type> points;
                boost::geometry::intersection(
                    intersectable,
                    *edge,
                    points
                );
                for (auto intersecting: points) {
                    result.push_back(std::make_pair(edge.get(), intersecting));
                }
            }
            return result;
        }
        size_t passed_corners(
            linestring_type const* const from,
            linestring_type const* const to
        ) const {
            linestring_type const* checked = from;
            size_t result = 0;
            do {
                if (to == checked) {
                    break;
                }
                checked = order.at(checked).first;
                result++;
                if (from == checked) {
                    throw std::logic_error("unreachable edge passed");
                }
            } while (true);
            return result;
        }
    private:
        std::vector<
            std::shared_ptr<point_type>
        > corners;
        std::vector<
            std::shared_ptr<linestring_type>
        > edges;
        std::map<
            linestring_type const*,
            std::pair<linestring_type const*, point_type const*>
        > order;
    };

    template<typename differentiator>
    class critical_point_mapper :
    protected eigenvalue_evaluator<differentiator> {
        typedef Point<differentiator::InputsAtCompileTime> point_type;
        typedef Vector<differentiator::InputsAtCompileTime> vector_type;
        typedef boost::geometry::model::point<
            double,
            2,
            boost::geometry::cs::cartesian
        > geometry_point;
        typedef boost::geometry::model::linestring<geometry_point> linestring;
    public:
        critical_point_mapper(
            differentiator& differentiating,
            datastructs::Dimensions dimensions
        ) :
        eigenvalue_evaluator<differentiator>(differentiating),
        dimensions(dimensions) {
        }
        void add_sample_point(
            Point<differentiator::InputsAtCompileTime> const& sample
        ) {
            try {
                datastructs::CriticalPoint found;
                if (
                    this->find_critical_point(
                        sample,
                        found.point,
                        found.type,
                        found.eigenvectors
                    )
                ) {
                    std::shared_ptr<datastructs::CriticalPoint> inserted(
                        new datastructs::CriticalPoint(found)
                    );
                    critical_points.insert(
                        found.point,
                        inserted
                    );
                }
            } catch (std::exception& caught) {
                std::cout << "failed: " << caught.what() << "\n";
            }
        }
        std::vector<datastructs::CriticalPoint> get_critical_points() const {
            auto found = critical_points.as_iteratable();
            discretization<double> const voronoi_scale(
                std::min(dimensions.xMin, dimensions.yMin),
                std::max(dimensions.xMax, dimensions.yMax)
            );
            std::vector<geometry_point> boundary_corners;
            boundary_corners.push_back(
                geometry_point(dimensions.xMin, dimensions.yMin)
            );
            boundary_corners.push_back(
                geometry_point(dimensions.xMax, dimensions.yMin)
            );
            boundary_corners.push_back(
                geometry_point(dimensions.xMax, dimensions.yMax)
            );
            boundary_corners.push_back(
                geometry_point(dimensions.xMin, dimensions.yMax)
            );
            bounding_box<geometry_point, linestring> boundaries(
                boundary_corners
            );
            double const always_outside(
                2 * std::max(
                    dimensions.xMax - dimensions.xMin,
                    dimensions.yMax - dimensions.yMin
                )
            );
            Matrix2 turn90;
            turn90(0, 0) = 0.0;
            turn90(0, 1) = 1.0;
            turn90(1, 0) = -1.0;
            turn90(1, 1) = 0.0;
            std::vector<
                std::pair<
                    Point<differentiator::InputsAtCompileTime>,
                    std::shared_ptr<datastructs::CriticalPoint>
                >
            > found_vector(found.begin(), found.end());
            std::vector<point> voronoi_points;
            for (
                size_t i = 0;
                found_vector.size() > i;
                i++
            ) {
                auto checking = &found_vector[i];
                voronoi_points.push_back(
                    point(
                        voronoi_scale.to_int(checking->first(0)),
                        voronoi_scale.to_int(checking->first(1))
                    )
                );
            }
            std::vector<segment> voronoi_segments;
            boost::polygon::voronoi_diagram<double> voronoi;
            construct_voronoi(
                voronoi_points.begin(),
                voronoi_points.end(),
                voronoi_segments.begin(),
                voronoi_segments.end(),
                &voronoi
            );
            std::vector<datastructs::CriticalPoint> result;
            for (
                boost::polygon::voronoi_diagram<
                    double
                >::const_cell_iterator traversing = voronoi.cells().begin();
                voronoi.cells().end() != traversing;
                traversing++
            ) {
                if (!traversing->contains_point()) {
                    continue;
                }
                if (
                    traversing->source_category(
                    ) != boost::polygon::SOURCE_CATEGORY_SINGLE_POINT
                ) {
                    continue;
                }
                datastructs::CriticalPoint critical_point = *found_vector[
                    traversing->source_index()
                ].second;
                std::vector<datastructs::Line>& voronoi_lines(
                    critical_point.voronoi_lines
                );
                auto finished = traversing->incident_edge();
                auto edge = traversing->incident_edge();
                do {
                    auto next = edge->next();
                    auto from_voronoi = next->vertex0();
                    auto to_voronoi = next->vertex1();
                    if (from_voronoi) {
                        point_type const from(
                            voronoi_scale.to_real(from_voronoi->x()),
                            voronoi_scale.to_real(from_voronoi->y())
                        );
                        if (within_boundaries(from)) {
                            break;
                        }
                        edge = next;
                        finished = finished->next();
                        assert(traversing->incident_edge() != finished);
                        continue;
                    }
                    if (!to_voronoi) {
                        break;
                    }
                    point_type const to(
                        voronoi_scale.to_real(to_voronoi->x()),
                        voronoi_scale.to_real(to_voronoi->y())
                    );
                    if (within_boundaries(to)) {
                        break;
                    }
                    edge = next;
                    finished = finished->next();
                    assert(traversing->incident_edge() != finished);
                    continue;
                } while (true);
                linestring const* exited = NULL;
                do {
                    edge = edge->next();
                    auto from_voronoi = edge->vertex0();
                    auto to_voronoi = edge->vertex1();
                    datastructs::Line voronoi_line;
                    if (from_voronoi) {
                        voronoi_line.a(0) = voronoi_scale.to_real(
                            from_voronoi->x()
                        );
                        voronoi_line.a(1) = voronoi_scale.to_real(
                            from_voronoi->y()
                        );
                    }
                    if (to_voronoi) {
                        voronoi_line.b(0) = voronoi_scale.to_real(
                            to_voronoi->x()
                        );
                        voronoi_line.b(1) = voronoi_scale.to_real(
                            to_voronoi->y()
                        );
                    }
                    if (exited) {
                        if (!to_voronoi) {
                            continue;
                        }
                        if (!within_boundaries(voronoi_line.b)) {
                            continue;
                        }
                    }
                    if (from_voronoi && to_voronoi) {
                        linestring next;
                        next.push_back(
                            geometry_point(voronoi_line.a(0), voronoi_line.a(1))
                        );
                        next.push_back(
                            geometry_point(voronoi_line.b(0), voronoi_line.b(1))
                        );
                        auto intersecting = boundaries.intersection(
                            next
                        );
                        if (!intersecting.empty()) {
                            if (exited) {
                                voronoi_line.a(0) = intersecting[
                                    0
                                ].second.get<0>();
                                voronoi_line.a(1) = intersecting[
                                    0
                                ].second.get<1>();
                                // TODO: connect exit and entry point
                                exited = NULL;
                            } else {
                                voronoi_line.b(0) = intersecting[
                                    0
                                ].second.get<0>();
                                voronoi_line.b(1) = intersecting[
                                    0
                                ].second.get<1>();
                                exited = intersecting[0].first;
                            }
                        }
                        voronoi_lines.push_back(voronoi_line);
                        continue;
                    }
                    point_type const other = found_vector[
                        edge->twin()->cell()->source_index()
                    ].second->point;
                    vector_type edge_direction = normalized(
                        turn90 * (other - critical_point.point)
                    );
                    datastructs::Line last_line;
                    if (voronoi_lines.empty()) {
                        auto next_edge = edge->next();
                        auto next_finished = finished->next();
                        if (traversing->incident_edge() != next_finished) {
                            edge = next_edge;
                            finished = next_finished;
                            continue;
                        }
                        point_type const between = (
                            critical_point.point + other
                        ) / 2.0;
                        if (!from_voronoi && !to_voronoi) {
                            point_type const outside_one(
                                between + always_outside * edge_direction
                            );
                            point_type const outside_other(
                                between - always_outside * edge_direction
                            );
                            linestring exceeding;
                            exceeding.push_back(
                                geometry_point(outside_one(0), outside_one(1))
                            );
                            exceeding.push_back(
                                geometry_point(
                                    outside_other(0),
                                    outside_other(1)
                                )
                            );
                            auto intersecting = boundaries.intersection(
                                exceeding
                            );
                            assert(2 == intersecting.size());
                            voronoi_line.a = point_type(
                                intersecting[0].second.get<0>(),
                                intersecting[0].second.get<1>()
                            );
                            voronoi_line.b = point_type(
                                intersecting[1].second.get<0>(),
                                intersecting[1].second.get<1>()
                            );
                            voronoi_lines.push_back(voronoi_line);
                            continue;
                        }
                        if (from_voronoi) {
                            last_line.a = between;
                            last_line.b = voronoi_line.a;
                        }
                        if (to_voronoi) {
                            last_line.a = voronoi_line.b;
                            last_line.b = between;
                        }
                    } else {
                        last_line = voronoi_lines.back();
                    }
                    vector_type const last = normalized(
                        last_line.b - last_line.a
                    );
                    if (
                        0 < (
                            edge_direction(0) * last(1)
                        ) - (
                            last(0) * edge_direction(1)
                        )
                    ) {
                        edge_direction = -1 * edge_direction;
                    }
                    if (from_voronoi) {
                        bool const try_opposite = -0.9 > last * edge_direction;
                        point_type const outside(
                            voronoi_line.a + always_outside * edge_direction
                        );
                        linestring exceeding;
                        assert(!exited);
                        if (try_opposite) {
                            point_type const outside_other(
                                voronoi_line.a - always_outside * edge_direction
                            );
                            exceeding.push_back(
                                geometry_point(
                                    outside_other(0),
                                    outside_other(1)
                                )
                            );
                        }
                        exceeding.push_back(
                            geometry_point(voronoi_line.a(0), voronoi_line.a(1))
                        );
                        exceeding.push_back(
                            geometry_point(outside(0), outside(1))
                        );
                        auto intersecting = boundaries.intersection(
                            exceeding
                        );
                        assert(0 < intersecting.size());
                        size_t index = 0;
                        if (try_opposite) {
                            assert(1 < intersecting.size());
                            index = (
                                norm(
                                    voronoi_line.a - point_type(
                                        intersecting[0].second.get<0>(),
                                        intersecting[0].second.get<1>()
                                    )
                                ) < norm(
                                    voronoi_line.a - point_type(
                                        intersecting[1].second.get<0>(),
                                        intersecting[1].second.get<1>()
                                    )
                                )
                            ) ? 0 : 1;
                        }
                        voronoi_line.b = point_type(
                            intersecting[index].second.get<0>(),
                            intersecting[index].second.get<1>()
                        );
                        exited = intersecting[index].first;
                    } else {
                        assert(to_voronoi);
                        point_type const outside(
                            voronoi_line.b - always_outside * edge_direction
                        );
                        linestring exceeding;
                        if (exited) {
                            point_type const outside_other(
                                voronoi_line.b + always_outside * edge_direction
                            );
                            exceeding.push_back(
                                geometry_point(
                                    outside_other(0),
                                    outside_other(1)
                                )
                            );
                        }
                        exceeding.push_back(
                            geometry_point(voronoi_line.b(0), voronoi_line.b(1))
                        );
                        exceeding.push_back(
                            geometry_point(outside(0), outside(1))
                        );
                        auto intersecting = boundaries.intersection(
                            exceeding
                        );
                        assert(0 < intersecting.size());
                        size_t index = 0;
                        if (exited) {
                            assert(1 < intersecting.size());
                            index = (
                                boundaries.passed_corners(
                                    exited,
                                    intersecting[0].first
                                ) < boundaries.passed_corners(
                                    exited,
                                    intersecting[1].first
                                )
                            ) ? 0 : 1;
                        }
                        // TODO: connect exit and entry point
                        exited = NULL;
                        voronoi_line.a = point_type(
                            intersecting[index].second.get<0>(),
                            intersecting[index].second.get<1>()
                        );
                    }
                    voronoi_lines.push_back(voronoi_line);
                } while (
                    finished != edge
                );
                if (voronoi_lines.empty()) {
                    // do not return points without voronoi cell lines
                    // TODO: compute applicable lines for infinite cases
                    continue;
                }
                result.push_back(critical_point);
            }
            return result;
        }
    protected:
        bool within_boundaries(point_type checked) const {
            if (checked(0) < dimensions.xMin) {
                return false;
            }
            if (checked(0) > dimensions.xMax) {
                return false;
            }
            if (checked(1) < dimensions.yMin) {
                return false;
            }
            if (checked(1) > dimensions.yMax) {
                return false;
            }
            return true;
        }
    private:
        datastructs::Dimensions dimensions;
        point_map<
            differentiator::InputsAtCompileTime,
            std::shared_ptr<datastructs::CriticalPoint>
        > critical_points;
    };
}
