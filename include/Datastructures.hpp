#ifndef DATASTRUCTURES_H_
#define DATASTRUCTURES_H_


#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

namespace datastructs
{
    struct Dimensions
    {
        float xMin;
        float yMin;
        float xMax;
        float yMax;
    };

    enum CriticalPointType
    {
        SOURCE,
        SINK,
        CENTER,
        SPIRAL,
        SADDLE
    };

    struct Eigenvector {
        Vector2 vector;
        std::complex<double> eigenvalue;
    };

    struct Line {
        Point2 a;
        Point2 b;
    };
    

    struct CriticalPoint {
        Point2 point;
        CriticalPointType type;
        std::vector<Eigenvector> eigenvectors;
        std::vector<Line> voronoi_lines; 
    };
}

#endif