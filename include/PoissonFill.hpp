#ifndef POISSONFILL_H_
#define POISSONFILL_H_

#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Datastructures.hpp>

#include "poisson_disk_sampling.h"


namespace poisson
{

    class PoissonFiller
    {
    private:
        std::vector<std::array<double, 2>> get_poisson_dist();
        // private:
        //     std::vector<std::array<double, 2>> get_poisson_dist();

    public:
        double poisson_radius;
        double template_influence_radius_ratio;
        datastructs::Dimensions dimensions;
        PoissonFiller(datastructs::Dimensions dimensions, double poisson_radius, double template_influence_radius_ratio);

        std::vector<Point2> get_poisson_fill_points(std::vector<datastructs::CriticalPoint> *critical_points);
    };
};

#endif