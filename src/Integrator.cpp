
#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Integrator.hpp>
#include <Utils.hpp>

namespace integrator
{

    std::vector<
        Point<2>>
    Integrator::line_points(
        Point<2> start,
        int max_length)
    {
        std::vector<
            Point<2>>
            result;
        do
        {
            result.push_back(start);
            max_length--;
            if (0 > max_length)
            {
                break;
            }
            Point<2> next;
            if (!this->get_next_value(start, next) || this->check_termination(next))
            {
                break;
            }
            start = next;
        } while (
            true);
        return result;
    }

    void Integrator::append_streamlines_for_seeds(std::vector<Point2> *seeds)
    {

        for (auto seed : *seeds)
        {
            auto line_points = this->line_points(seed, this->integration_iterations);
            std::vector<size_t> line(line_points.size());

            typename std::vector<Point<2>>::const_iterator reading = line_points.begin();

            typename std::vector<size_t>::iterator writing = line.begin();

            for (; line_points.end() != reading; reading++, writing++)
            {
                *writing = this->line_set->addPoint(*reading);
            }

            this->line_set->addLine(line);
        }
    }

    bool Integrator::get_value(
        Point<2> input,
        Tensor<double, 2> &output)
    {
        if (evaluator->reset(input))
        {
            auto v = evaluator->value();
            output = v;
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Integrator::check_termination(Point2 point)
    {

        for (int i = 0; i < this->line_set->numPoints(); i++)
        {
            if (utils::get_point_distance(point, this->line_set->points().access(i)) < this->delta_streamlines)
            {
                return true;
            }
        }
        return false;
    }

    bool RungeKuttaIntegrator::get_next_value(
        Point<2> last,
        Point<2> &next)
    {
        Tensor<double, 2> k1;
        if (!this->get_value(last, k1))
        {
            return false;
        }
        Tensor<double, 2> k2;
        if (!this->get_value(last + 0.5 * this->step_size * k1, k2))
        {
            return false;
        }
        Tensor<double, 2> k3;
        if (!this->get_value(last + 0.5 * step_size * k2, k3))
        {
            return false;
        }
        Tensor<double, 2> k4;
        if (!this->get_value(last + step_size * k3, k4))
        {
            return false;
        }
        next = last + (k1 + 2 * k2 + 2 * k3 + k4) * step_size / 6.0;
        return true;
    }

    bool AdaptiveEulerIntegrator::get_next_value(
        Point<2> last,
        Point<2> &next)
    {
        Tensor<double, 2> full;
        if (!this->get_value(last, full))
        {
            return false;
        }
        Point<2> const next_full = last + full * step_size;
        Tensor<double, 2> half_first;
        if (!this->get_value(last, half_first))
        {
            return false;
        }
        Tensor<double, 2> half_second;
        if (
            !this->get_value(
                last + 0.5 * step_size * half_first,
                half_second))
        {
            return false;
        }
        Point<2> const next_half(
            last + 0.5 * (half_first + half_second) * step_size);
        Scalar const difference(norm(next_full - next_half));
        if (threshold_double > difference)
        {
            step_size *= 2.0;
        }
        if (threshold_half < difference)
        {
            step_size *= 0.5;
            return this->get_next_value(last, next);
        }
        next = next_full;
        return true;
    }
}