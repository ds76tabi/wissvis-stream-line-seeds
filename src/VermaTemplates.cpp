
#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Datastructures.hpp>
#include <Utils.hpp>
#include <VermaTemplates.hpp>

using namespace fantom;

namespace verma
{

    std::vector<Point2> VermaTemplateGenerator::create_points_on_circle(Point2 center, double radius, Point2 startpoint, double delta_seed)
    {

        std::vector<Point2> result_points;
        double circumference = 2 * M_PI * radius;
        double point_distance = delta_seed;
        // Was passiert wenn delta_seed > radius??

        double angle = std::atan2(startpoint[1] - center[1], startpoint[0] - center[0]);
        int numPoints = (int)(circumference / point_distance);
        double angle_change = 2 * M_PI / numPoints;

        for (int i = 0; i < numPoints; i++)
        {
            double x = center[0] + radius * std::cos(angle);
            double y = center[1] + radius * std::sin(angle);

            Point2 point = {x, y};
            result_points.push_back(point);
            angle += angle_change;
        }

        return result_points;
    }

    std::vector<Point2> VermaTemplateGenerator::get_saddle_template_points(datastructs::CriticalPoint *critical_point)
    {
        std::vector<Point2> result_points;
        // find principle eigenVectors

        // utils::sort_eigenvectors_by_eigenvalue(&critical_point->eigenvectors);

        datastructs::Eigenvector first = critical_point->eigenvectors[0];

        // std::vector<datastructs::Eigenvector> principals = utils::get_principal_eigenvectors(critical_point->eigenvectors);
        // std::vector<datastructs::Eigenvector> ordered_principals = utils::order_vectors_by_angle(principals);
        //  find the Bisector of the Vectors

        // std::vector<Vector2> bisectors = utils::get_bisectors(ordered_principals);

        // find point where bisector hits voronoilines or boundary

        // create seedpoints till half of the line from critical point to voronoi line

        return result_points;
    }

    std::vector<Point2> VermaTemplateGenerator::get_source_sink_template_points(datastructs::CriticalPoint *critical_point)
    {
        std::vector<Point2> result_points;

        Point2 closest_point = utils::get_closest_voronoi_line_point(critical_point);
        Vector2 v = utils::get_vector_between_points(critical_point->point, closest_point);
        double radius = norm2(v) * this->seeding_distance_factor;
        Point2 first_seeding_point = critical_point->point + normalized(v) * radius;

        result_points = create_points_on_circle(critical_point->point, radius, first_seeding_point, this->delta_seed);

        return result_points;
    }

    std::vector<Point2> VermaTemplateGenerator::get_center_spiral_template_points(datastructs::CriticalPoint *critical_point)
    {
        // generates seed points in distance delta_seeds on the line from critical point to the closest boundary point

        std::vector<Point2> result_points;

        Point2 closest_point = utils::get_closest_voronoi_line_point(critical_point);

        double distance = utils::get_point_distance(critical_point->point, closest_point) * this->seeding_distance_factor;

        Vector2 vector_to_closest = utils::get_vector_between_points(critical_point->point, closest_point);

        // create shift vector of size delta_seed
        Vector2 shift_vector = this->delta_seed * normalized(vector_to_closest);

        Point2 current_point = critical_point->point;

        while ((utils::get_point_distance(critical_point->point, current_point) + this->delta_seed) <= distance)
        {
            Point2 new_point = current_point + shift_vector;
            result_points.push_back(new_point);
            current_point = new_point;
        }

        return result_points;
    }

    // VermaTemplateGenerator::VermaTemplateGenerator(double delta, double seeding_distance_factor = 0.5) : delta_seed(delta), seeding_distance_factor(seeding_distance_factor) {}

    std::vector<Point2> VermaTemplateGenerator::get_template_points(datastructs::CriticalPoint *critical_point)
    {

        std::vector<Point2> result;
        switch (critical_point->type)
        {
        case datastructs::CriticalPointType::SOURCE:
        case datastructs::CriticalPointType::SINK:
            /* code */
            result = this->get_source_sink_template_points(critical_point);
            break;

        case datastructs::CriticalPointType::SPIRAL:
        case datastructs::CriticalPointType::CENTER:
            result = this->get_center_spiral_template_points(critical_point);
            break;

        case datastructs::CriticalPointType::SADDLE:
            result = this->get_saddle_template_points(critical_point);
            break;
        }
        return result;
    }
}
