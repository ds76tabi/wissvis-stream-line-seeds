#include "poisson_disk_sampling.h"

#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <PoissonFill.hpp>
#include <Datastructures.hpp>
#include <Utils.hpp>

using namespace fantom;

namespace poisson
{

  std::vector<std::array<double, 2>> PoissonFiller::get_poisson_dist()
  {
    auto kXMin = std::array<double, 2>{{this->dimensions.xMin, this->dimensions.yMin}};
    auto kXMax = std::array<double, 2>{{this->dimensions.xMax, this->dimensions.yMax}};

    return thinks::PoissonDiskSampling(this->poisson_radius, kXMin, kXMax);
  }

  PoissonFiller::PoissonFiller(datastructs::Dimensions dimensions, double poisson_radius, double template_influence_radius_ratio)
  {
    this->poisson_radius = poisson_radius;
    this->template_influence_radius_ratio = template_influence_radius_ratio;
    this->dimensions = dimensions;
  }

  std::vector<Point2> PoissonFiller::get_poisson_fill_points(std::vector<datastructs::CriticalPoint> *critical_points)
  {
    std::vector<Point2> result;

    auto allPoissonPoints = this->get_poisson_dist();

    for (std::array<double, 2> pointArray : allPoissonPoints)
    {
      auto point = Point2(pointArray[0], pointArray[1]);

      bool isException = false;

      for (auto critical_point : *critical_points)
      {
        // use closest voronoi point as template size
        auto template_size = utils::get_point_distance(critical_point.point, utils::get_closest_voronoi_line_point(&critical_point));

        if (utils::get_point_distance(point, critical_point.point) < (this->template_influence_radius_ratio * template_size))
        {
          isException = true;
          break;
        }
      }

      if (!isException)
      {
        result.push_back(point);
      }
    }

    return result;
  }
}