
#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <Datastructures.hpp>

#include <cmath>

namespace utils
{

    std::string vector_to_string(Vector2 v) {
        return "[" + std::to_string(v[0]) + ", " + std::to_string(v[1]) + "]";
    }

    double dot_product(Vector2 v, Vector2 u)
    {
        return v * u;
    }

    double get_point_distance(Point2 p1, Point2 p2)
    {

        auto x_distance = p1[0] - p2[0];
        auto y_distance = p1[1] - p2[1];

        return std::sqrt((x_distance * x_distance) + (y_distance * y_distance));
    }

    Point2 get_middle_point(datastructs::Line line)
    {

        auto x = (line.a[0] + line.b[0]) / 2;
        auto y = (line.a[1] + line.b[1]) / 2;
        return Point2(x, y);
    }

    Point2 get_right_angle_point_on_line(Point2 *point, datastructs::Line * line) {

        // see the line as line.a + s * r
        // then try to find an scalar s for which the dot prodct from point to (line.a + s * r) and (line.a to (line.a + s * r)) equals zero
        
        Vector2 r = Vector2(line->a[0] - line->b[0], line->a[1] - line->b[1]); 

        double s = utils::dot_product((*point - line->a), r) / dot_product(r, r);

        return line->a + (s * r);

    }

    Point2 get_closest_voronoi_line_point(datastructs::CriticalPoint *critical_point)
    {

        // the closest point is always a middle point of a line

        auto closest_point = utils::get_right_angle_point_on_line(&critical_point->point,  &critical_point->voronoi_lines[0]);

        auto min_distance = utils::get_point_distance(critical_point->point, closest_point);

        for (int i = 1; i < critical_point->voronoi_lines.size(); i++)
        {
            auto line = critical_point->voronoi_lines[i];

            auto new_closest_point = utils::get_right_angle_point_on_line(&critical_point->point,  &critical_point->voronoi_lines[i]);

            auto new_distance = utils::get_point_distance(critical_point->point, new_closest_point);
            if (new_distance < min_distance)
            {
                min_distance = new_distance;
                closest_point = new_closest_point;
            }
        }

        return closest_point;
    }

    double get_furthest_point_distance(datastructs::CriticalPoint *cp, std::vector<Point2> *points)
    {

        double max_distance = get_point_distance(points->at(0), cp->point);

        for (int i = 1; i < points->size(); i++)
        {
            double new_distance = get_point_distance(cp->point, points->at(i));
            if (new_distance > max_distance)
            {
                max_distance = new_distance;
            }
        }

        return max_distance;
    }

    Vector2 get_vector_between_points(Point2 source, Point2 target)
    {
        return target - source;
    }

    bool compare_eigenvectors(datastructs::Eigenvector a, datastructs::Eigenvector b) { return a.eigenvalue.real() > b.eigenvalue.real(); }

    void sort_eigenvectors_by_eigenvalue(std::vector<datastructs::Eigenvector> *eigenvectors)
    {
        std::sort(eigenvectors->begin(), eigenvectors->end(), compare_eigenvectors);
    }

    // std::vector<datastructs::Eigenvector> get_principal_eigenvectors(std::vector<datastructs::Eigenvector> eigenvectors)
    // {

    //     std::vector<datastructs::Eigenvector> principals;
    //     std::vector<std::complex<double>> values;

    //     for (int i = 0; eigenvectors.size(); i++)
    //     {
    //         values.push_back(abs(eigenvectors[i].eigenvalue));
    //     }

    //     std::sort(values.begin(), values.end(), std::greater<std::complex<double>>());
    //     std::complex<double> max = values[0];

    //     for (int i = 0; eigenvectors.size(); i++)
    //     {
    //         if (abs(eigenvectors[i].eigenvalue) == max)
    //         {
    //             principals.push_back(eigenvectors[i]);
    //         }
    //     }

    //     return principals;
    // }

    double angle_between_vectors(Vector2 a, Vector2 b)
    {
        double scalar = a[0] * b[0] + a[1] * b[1];
        double aMagnitude = norm2(a);
        double bMagnitude = norm2(b);

        double angle = std::acos(scalar / (aMagnitude * bMagnitude));

        return angle;
    }

    std::vector<datastructs::Eigenvector> order_vectors_by_angle(std::vector<datastructs::Eigenvector> principals)
    {

        std::vector<datastructs::Eigenvector> ordered_principals = principals;
        std::vector<double> angles;
        angles.push_back(angle_between_vectors(principals[0].vector, principals[1].vector));
        angles.push_back(angle_between_vectors(principals[0].vector, principals[2].vector));
        angles.push_back(angle_between_vectors(principals[0].vector, principals[3].vector));

        int index = std::distance(std::begin(angles), std::min_element(std::begin(angles), std::end(angles)));

        std::swap(ordered_principals[1], ordered_principals[index]);

        return ordered_principals;
    }

    Vector2 get_bisectors(Vector2 v, Vector2 u)
    {
        return normalized(v) + normalized(u);
    }

    std::vector<Vector2> get_bisectors(std::vector<datastructs::Eigenvector> principals)
    {
    }

}