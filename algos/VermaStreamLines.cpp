#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>

#include <VermaTemplates.hpp>

#include <Eigen/Eigenvalues>
#include <unsupported/Eigen/src/NumericalDiff/NumericalDiff.h>

#include <PoissonFill.hpp>
#include <Datastructures.hpp>
#include <Utils.hpp>
#include <CriticalPoints.hpp>
#include <Integrator.hpp>

#include <iostream>

using namespace fantom;

namespace
{
    class VermaStreamLines : public DataAlgorithm
    {

    public:
        struct Options : public DataAlgorithm::Options
        {
            Options(fantom::Options::Control &control)
                : DataAlgorithm::Options(control)
            {
                // add<Field< 2, Tensor<double, 2>>>(
                //     "Field",
                //     "A vector field",
                //     definedOn<Grid<2>>(Grid<2>::Points)
                // );

                add<Field<2, Vector2>>("Field", "A 2D field filled with 2D vectors");

                // TODO: search paper for params
                add<double>("delta_seed", "", 1.0);
                add<double>("delta_streamline", "", 1.0);
                add<double>("seeding_distance_factor", "", 0.5);
                addSeparator();
                add<double>("poisson_radius", "", 1.0);
                add<double>("template_influence_ratio", "", 0.8);
                addSeparator();
                add<int>("integration_steps", "", 1000);
                add<double>("integration_step_size", "", 0.1);
            }
        };

        struct DataOutputs : public DataAlgorithm::DataOutputs
        {
            DataOutputs(fantom::DataOutputs::Control &control)
                : DataAlgorithm::DataOutputs(control)
            {
                add<const LineSet<2>>("streamLines");
                add<const LineSet<2>>("voronoiLines");
                add<const LineSet<2>>("criticalPoints");
                add<const LineSet<2>>("templatePoints");
                add<const LineSet<2>>("poissonPoints");
            }
        };

        void log_critical_points(std::vector<datastructs::CriticalPoint> * critical_points) {

            std::map<datastructs::CriticalPointType, int> result_map;

            result_map =  {
                {datastructs::CriticalPointType::CENTER, 0},
                {datastructs::CriticalPointType::SADDLE, 0},
                {datastructs::CriticalPointType::SPIRAL, 0},
                {datastructs::CriticalPointType::SINK, 0},
                {datastructs::CriticalPointType::SOURCE, 0}
            };

            for (auto cp : *critical_points) {
                result_map[cp.type] = result_map[cp.type] +1;
            }

            int total_number = 0;

            for (auto kv_pair : result_map) {
                debugLog() << "Found " << kv_pair.second << " critical points of type " << kv_pair.first << std::endl;
                total_number += kv_pair.second;
            }

            debugLog() << "Found critical points: " << total_number << std::endl;
        }


        LineSet<2> parse_voronoi_lines(std::vector<datastructs::CriticalPoint> *critical_points) {

            LineSet<2> voronoi_lines = LineSet<2>();

            size_t vector_index = 0;

            for (auto cp: *critical_points) {

                for (datastructs::Line line : cp.voronoi_lines) {
                    std::vector<size_t> line_indices;

                    voronoi_lines.addPoint(line.a);
                    line_indices.push_back(vector_index);
                    vector_index++;
                    
                    voronoi_lines.addPoint(line.b);
                    line_indices.push_back(vector_index);
                    vector_index++;

                    voronoi_lines.addLine(line_indices);
                }
            }
            return voronoi_lines;
        }

        VermaStreamLines(InitData &data) : DataAlgorithm(data) {}

        virtual void execute(const Algorithm::Options &options, const volatile bool & /*abortFlag*/) override
        {

            LineSet<2> result = LineSet<2>();

            std::shared_ptr<const Field<2, Vector2>> field = options.get<Field<2, Vector2>>("Field");
            std::shared_ptr<const Function<Vector2>> function = options.get<Function<Vector2>>("Field");

            double delta_seed = options.get<double>("delta_seed");
            double delta_streamline = options.get<double>("delta_streamline");
            double seeding_distance_factor = options.get<double>("seeding_distance_factor");

            double poisson_radius = options.get<double>("poisson_radius");
            double template_influence_ratio = options.get<double>("template_influence_ratio");

            int integration_steps = options.get<int>("integration_steps");
            Scalar integration_step_size(options.get<double>("integration_step_size"));

            std::shared_ptr<
                Grid<2> const>
                grid = std::dynamic_pointer_cast<Grid<2> const>(
                    function->domain());
            if (!grid)
            {
                throw std::logic_error("incompatible grid");
            }

            PointSetBase::BoundingBox const boundaries = grid->getBoundingBox();

            datastructs::Dimensions dimensions = {};

            enum
            {
                fantom_x = 0,
                fantom_y = 1,
            };

            dimensions.xMin = boundaries[fantom_x].first;
            dimensions.xMax = boundaries[fantom_x].second;
            dimensions.yMin = boundaries[fantom_y].first;
            dimensions.yMax = boundaries[fantom_y].second;

            std::shared_ptr<
                FieldEvaluator<2, Vector2>>
                evaluator = field->makeEvaluator();

            typedef Eigen::NumericalDiff<
                evaluator_eigen_functor_wrapper<2>>
                differentiator;

            differentiator with_jacobian(evaluator);

            critical_point_mapper<differentiator> mapping(
                with_jacobian,
                dimensions);

            ValueArray<Point2> const &points = grid->points();

            for (size_t i = 0; i < grid->numCells(); ++i)
            {
                Cell const cell = grid->cell(i);
                Point2 point;
                for (size_t j = 0; j < cell.numVertices(); ++j)
                {
                    point += points[cell.index(j)];
                }
                point /= cell.numVertices();
                mapping.add_sample_point(point);
            }

            std::vector<datastructs::CriticalPoint> critical_points(
                mapping.get_critical_points());

            // debug critical points
            
            log_critical_points(&critical_points);

            // TODO get template points for critials
            std::vector<Point2> template_points;

            // generate verma templates

            auto verma_generator = verma::VermaTemplateGenerator(delta_seed, seeding_distance_factor);

            for (datastructs::CriticalPoint critical_point : critical_points)
            {
                auto new_template_points = verma_generator.get_template_points(&critical_point);
                template_points.insert(template_points.end(), new_template_points.begin(), new_template_points.end());
            }

            // generate integrator

            auto terminating_integrator = integrator::RungeKuttaIntegrator(field, std::make_shared<LineSet<2>>(result), delta_streamline, integration_steps, integration_step_size);

            // add the streamlines to the lineset (and TERMINATE)

            terminating_integrator.append_streamlines_for_seeds(&template_points);

            // fill up with poisson

            auto poisson_filler = poisson::PoissonFiller(dimensions, poisson_radius, template_influence_ratio);

            std::vector<Point2> poisson_points = poisson_filler.get_poisson_fill_points(&critical_points);

            // generate streamlines for poisson points (and TERMINATE)

            terminating_integrator.append_streamlines_for_seeds(&poisson_points);

            setResult("streamLines", std::make_shared<LineSet<2>>(result));

            // also return the critical points

            LineSet<2> critical_point_set = LineSet<2>();

            for (auto cp: critical_points) {
                critical_point_set.addPoint(cp.point);
            }

            setResult("criticalPoints", std::make_shared<LineSet<2>>(critical_point_set));


            //also show the voronoi lines
            
            setResult("voronoiLines", std::make_shared<LineSet<2>>(parse_voronoi_lines(&critical_points)));

            // also show the template points

            LineSet<2> template_point_set = LineSet<2>();

            for (auto p: template_points) {
                template_point_set.addPoint(p);
            }

            setResult("templatePoints", std::make_shared<LineSet<2>>(template_point_set));

            // also return the poisson points

            LineSet<2> poisson_point_set = LineSet<2>();

            for (auto pp : poisson_points) {
                poisson_point_set.addPoint(pp);
            }

            setResult("poissonPoints", std::make_shared<LineSet<2>>(poisson_point_set));


            
        }
    };

    AlgorithmRegister<VermaStreamLines> dummy("Hauptaufgabe/VermaStreamLines", "Generate Streamlines according to the Verma paper");
}
