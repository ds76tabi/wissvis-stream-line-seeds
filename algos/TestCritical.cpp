// Description: draw voronoi tesselation and eigenvectors for critical points
//  from a vector field
// Author: Isidor Zeuner
// iz32mega@studserv.uni-leipzig.de
// For license see: https://mit-license.org
// -------------------------------------------------------------------

#include <fantom-plugins/utils/Graphics/ObjectRenderer.hpp>

#include <Eigen/Eigenvalues>
#include <unsupported/Eigen/src/NumericalDiff/NumericalDiff.h>

#include <CriticalPoints.hpp>

using namespace fantom;

namespace
{
    template<int D>
    class FieldTopologyAlgorithm : public VisAlgorithm
    {
    public:
        struct Options : public VisAlgorithm::Options
        {
            Options( fantom::Options::Control& control )
                : VisAlgorithm::Options( control )
            {
                add<
                    Field<
                        D,
                        Tensor<double, D>
                    >
                >(
                    "Field",
                    "A vector field",
                    definedOn<
                        Grid<D>
                    >(Grid<D>::Points)
                );
                add<double>(
                    "radius",
                    "The cadius of the marking circles.",
                    0.05
                );
                add<Color>(
                    "source color",
                    "The color of source-type critical points.",
                    Color(0.0, 1.0, 0.0)
                );
                add<Color>(
                    "sink color",
                    "The color of sink-type critical points.",
                    Color(1.0, 0.0, 0.0)
                );
                add<Color>(
                    "saddle color",
                    "The color of saddle-type critical points.",
                    Color(0.0, 0.0, 1.0)
                );
                add<Color>(
                    "spiral color",
                    "The color of spiral-type critical points.",
                    Color(0.5, 0.5, 0.0)
                );
                add<Color>(
                    "center color",
                    "The color of center-type critical points.",
                    Color(0.5, 0.5, 1.0)
                );
            }
        };

        struct VisOutputs : public VisAlgorithm::VisOutputs
        {
            VisOutputs( fantom::VisOutputs::Control& control )
                : VisAlgorithm::VisOutputs( control )
            {
                addGraphics( "marked" );
                addGraphics( "voronoi" );
            }
        };


        FieldTopologyAlgorithm( InitData& data )
            : VisAlgorithm( data )
        {
        }

        virtual void execute( const Algorithm::Options& options, const volatile bool& /*abortFlag*/ ) override
        {
            std::shared_ptr<
                Field<
                    D,
                    Tensor<double, D>
                > const
            > field = options.get<
                Field<
                    D,
                    Tensor<double, D>
                >
            >("Field");

            std::shared_ptr<
                Function<
                    Tensor<double, D>
                > const
            > function = options.get<
                Function<
                    Tensor<double, D>
                >
            >("Field");

            double const radius = options.get<double>("radius");
            Color const source_color = options.get<Color>("source color");
            Color const sink_color = options.get<Color>("sink color");
            Color const saddle_color = options.get<Color>("saddle color");
            Color const spiral_color = options.get<Color>("spiral color");
            Color const center_color = options.get<Color>("center color");

            std::shared_ptr<
                const Grid<D>
            > grid = std::dynamic_pointer_cast<Grid<D> const>(
                function->domain()
            );
            if(!grid) {
                throw std::logic_error("incompatible grid");
            }

            PointSetBase::BoundingBox const boundaries = grid->getBoundingBox();

            enum {
                fantom_x = 0,
                fantom_y = 1,
            };

            datastructs::Dimensions dimensions;

            dimensions.xMin = boundaries[fantom_x].first;
            dimensions.xMax = boundaries[fantom_x].second;
            dimensions.yMin = boundaries[fantom_y].first;
            dimensions.yMax = boundaries[fantom_y].second;

            std::shared_ptr<
                FieldEvaluator<
                    D,
                    Tensor<double, D>
                >
            > evaluator = field->makeEvaluator();

            typedef Eigen::NumericalDiff<
                evaluator_eigen_functor_wrapper<D>
            > differentiator;

            differentiator with_jacobian(evaluator);

            critical_point_mapper<differentiator> mapping(
                with_jacobian,
                dimensions
            );

            ValueArray<
                Point<D>
            > const& points = grid->points();

            for(size_t i = 0; i < grid->numCells(); ++i) {
                Cell const cell = grid->cell(i);
                Point<D> point;
                for (size_t j = 0; j < cell.numVertices(); ++j) {
                    point += points[cell.index(j)];
                }
                point /= cell.numVertices();
                mapping.add_sample_point(point);
            }

            auto const& system = graphics::GraphicsSystem::instance();

            auto markers = std::make_shared<graphics::ObjectRenderer>(system);

            auto voronoi_edges = std::make_shared<graphics::ObjectRenderer>(
                system
            );

            std::vector<datastructs::CriticalPoint> critical_points(
                mapping.get_critical_points()
            );

            for (
                auto critical_point = critical_points.begin();
                critical_points.end() != critical_point;
                critical_point++
            ) {
                Color type_color;
                switch (critical_point->type) {
                case datastructs::SOURCE:
                    type_color = source_color;
                    break;
                case datastructs::SINK:
                    type_color = sink_color;
                    break;
                case datastructs::SADDLE:
                    type_color = saddle_color;
                    break;
                case datastructs::SPIRAL:
                    type_color = spiral_color;
                    break;
                case datastructs::CENTER:
                    type_color = center_color;
                    break;
                default:
                    throw std::logic_error("unknown classification");
                }
                markers->addSphere(
                    point_embedding<D>::embed_3d(critical_point->point),
                    radius,
                    type_color
                );
                for (
                    auto drawing = critical_point->eigenvectors.begin();
                    critical_point->eigenvectors.end() != drawing;
                    drawing++
                ) {
                    markers->addArrow(
                        point_embedding<D>::embed_3d(critical_point->point),
                        point_embedding<D>::embed_3d(drawing->vector),
                        radius / 10.0,
                        type_color,
                        true
                    );
                }
                for (
                    auto drawing = critical_point->voronoi_lines.begin();
                    critical_point->voronoi_lines.end() != drawing;
                    drawing++
                ) {
                    std::cout << "DEBUG voronoi edge " << drawing->a << " ";
                    std::cout << drawing->b << "\n";
                    voronoi_edges->addCylinder(
                        point_embedding<D>::embed_3d(
                            (drawing->b + drawing->a) / 2.0
                        ),
                        point_embedding<D>::embed_3d(drawing->b - drawing->a),
                        radius / 20.0,
                        Color(1.0, 0.0, 0.0)
                    );
                }
            }

            setGraphics("marked", markers->commit());
            setGraphics("voronoi", voronoi_edges->commit());
        }
    };

    AlgorithmRegister<
        FieldTopologyAlgorithm<2>
    > integrator2d("Practical/Seeding2D", "Compute critical points in 2D");

    /*
    AlgorithmRegister<
        FieldTopologyAlgorithm<3>
    > integrator3d("Practical/Seeding3D", "Compute critical points in 3D");
    */
}
