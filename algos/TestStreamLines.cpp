// Description: compute stream lines using adaptive Euler and Runge-Kutta
//  integrators
// Author: Isidor Zeuner
// iz32mega@studserv.uni-leipzig.de
// For license see: https://mit-license.org
// -------------------------------------------------------------------

#include <fantom/algorithm.hpp>
#include <fantom/dataset.hpp>
#include <fantom/register.hpp>
#include <fantom/datastructures/domains/LineSet.hpp>

#define WITH_UBUNTU 1

using namespace fantom;

namespace
{
#ifdef WITH_UBUNTU
    template<int D>
    struct lineset_chooser {
        typedef LineSet<D> type;
        typedef type exported_type;
        static std::shared_ptr<exported_type const> do_export(
            std::shared_ptr<type> exporting
        ) {
            return exporting;
        }
    };
#endif
#ifndef WITH_UBUNTU
    template<int D>
    struct point_embedding {
        static Point<3> embed_3d(Point<D> original) {
            Point<3> result;
            for (size_t i = 0; i < D; i++) {
                result(i) = original(i);
            }
            return result;
        }
        static Point<D> extract_3d(Point<3> embedded) {
            Point<D> result;
            for (size_t i = 0; i < D; i++) {
                result(i) = embedded(i);
            }
            return result;
        }
    };

    template<int D>
    class lineset_embedding {
    public:
        lineset_embedding() :
        lineset3(new LineSet) {
        }
        size_t addPoint(Point<D> added) {
            return lineset3->addPoint(point_embedding<D>::embed_3d(added));
        }
        size_t addLine(std::vector<size_t> added) {
            return lineset3->addLine(added);
        }
        std::shared_ptr<LineSet> get_representation() const {
            return lineset3;
        }
    private:
        std::shared_ptr<LineSet> lineset3;
    };

    template<int D>
    struct lineset_chooser {
        typedef lineset_embedding<D> type;
        typedef LineSet exported_type;
        static std::shared_ptr<exported_type const> do_export(
            std::shared_ptr<type> exporting
        ) {
            return exporting->get_representation();
        }
    };

    template<>
    struct lineset_chooser<3> {
        typedef LineSet type;
        typedef type exported_type;
        static std::shared_ptr<exported_type const> do_export(
            std::shared_ptr<type> exporting
        ) {
            return exporting;
        }
    };
#endif

    template<int D>
    class StreamlineIntegrationAlgorithm : public DataAlgorithm
    {
        class Integrator {
        public:
            Integrator(
                std::shared_ptr<
                    Field<
                        D,
                        Tensor<double, D>
                    > const
                > field
            ) :
            field(
                field
            ),
            evaluator(
                field->makeEvaluator()
            ) {
            }
            virtual ~Integrator(
            ) {
            }
            virtual bool get_next_value(
                Point<D> last,
                Point<D>& next
            ) = 0;
            std::vector<
                Point<D>
            > line_points(
                Point<D> start,
                int max_length
            ) {
                std::vector<
                    Point<D>
                > result;
                do {
                    result.push_back(start);
                    max_length--;
                    if (0 > max_length) {
                        break;
                    }
                    Point<D> next;
                    if (!get_next_value(start, next)) {
                        break;
                    }
                    start = next;
                } while (
                    true
                );
                return result;
            }
        protected:
            bool get_value(
                Point<D> input,
                Tensor<double, D>& output
            ) {
                if (evaluator->reset(input)) {
                    auto v = evaluator->value();
                    output = v;
                    return true;
                } else {
                    return false;
                }
            }
        private:
            std::shared_ptr<
                Field<
                    D,
                    Tensor<double, D>
                > const
            > field;
            std::shared_ptr<
                FieldEvaluator<
                    D,
                    Tensor<double, D>
                >
            > evaluator;
        };

        class RungeKuttaIntegrator :
        public Integrator {
        public:
            RungeKuttaIntegrator(
                std::shared_ptr<
                    Field<
                        D,
                        Tensor<double, D>
                    > const
                > field,
                Scalar step_size
            ) :
            Integrator(
                field
            ),
            step_size(
                step_size
            ) {
            }
            virtual bool get_next_value(
                Point<D> last,
                Point<D>& next
            ) {
                Tensor<double, D> k1;
                if (!this->get_value(last, k1)) {
                    return false;
                }
                Tensor<double, D> k2;
                if (!this->get_value(last + 0.5 * step_size * k1, k2)) {
                    return false;
                }
                Tensor<double, D> k3;
                if (!this->get_value(last + 0.5 * step_size * k2, k3)) {
                    return false;
                }
                Tensor<double, D> k4;
                if (!this->get_value(last + step_size * k3, k4)) {
                    return false;
                }
                next = last + (k1 + 2 * k2 + 2 * k3 + k4) * step_size / 6.0;
                return true;
            }
        private:
            Scalar const step_size;
        };

        class AdaptiveEulerIntegrator :
        public Integrator {
        public:
            AdaptiveEulerIntegrator(
                std::shared_ptr<
                    Field<
                        D,
                        Tensor<double, D>
                    > const
                > field,
                Scalar step_size,
                Scalar threshold_half,
                Scalar threshold_double
            ) :
            Integrator(
                field
            ),
            step_size(
                step_size
            ),
            threshold_half(
                threshold_half
            ),
            threshold_double(
                threshold_double
            ) {
            }
            virtual bool get_next_value(
                Point<D> last,
                Point<D>& next
            ) {
                Tensor<double, D> full;
                if (!this->get_value(last, full)) {
                    return false;
                }
                Point<D> const next_full = last + full * step_size;
                Tensor<double, D> half_first;
                if (!this->get_value(last, half_first)) {
                    return false;
                }
                Tensor<double, D> half_second;
                if (
                    !this->get_value(
                        last + 0.5 * step_size * half_first,
                        half_second
                    )
                ) {
                    return false;
                }
                Point<D> const next_half(
                    last + 0.5 * (half_first + half_second) * step_size
                );
                Scalar const difference(norm(next_full - next_half));
                if (threshold_double > difference) {
                    step_size *= 2.0;
                }
                if (threshold_half < difference) {
                    step_size *= 0.5;
                    return get_next_value(last, next);
                }
                next = next_full;
                return true;
            }
        private:
            Scalar step_size;
            Scalar const threshold_half;
            Scalar const threshold_double;
        };

    public:
        struct Options : public DataAlgorithm::Options
        {
            Options( fantom::Options::Control& control )
                : DataAlgorithm::Options( control )
            {
                add<
                    Field<
                        D,
                        Tensor<double, D>
                    >
                >(
                    "Field",
                    "A 3D vector field",
                    definedOn<
                        Grid<D>
                    >(Grid<D>::Points)
                );
                add<
                    Point<D>
                >("start", "start point",  Point<D>());
                add<
                    Tensor<double, D>
                >(
                    "slide delta",
                    "sliding vector",
                    Tensor<double, D>(0.01)
                );
                add<int>("slide steps", "", 100);
                add<InputChoices>(
                    "integrator",
                    "how to integrate stream lines",
                    std::vector<std::string>{"Runge-Kutta", "adaptive Euler"},
                    "Runge-Kutta"
                );
                add<int>(
                    "step count",
                    "number of integration steps",
                    100
                );
                add<double>(
                    "step size",
                    "(initial) integration step size",
                    0.1
                );
                add<double>(
                    "halving threshold",
                    "threshold for halving integration step size (Euler)",
                    0.1
                );
                add<double>(
                    "doubling threshold",
                    "threshold for doubling integration step size (Euler)",
                    0.01
                );
            }
        };

        struct DataOutputs : public DataAlgorithm::DataOutputs
        {
            DataOutputs( fantom::DataOutputs::Control& control )
                : DataAlgorithm::DataOutputs( control )
            {
		add<typename lineset_chooser<D>::exported_type const>(
                    "streamlines"
                );
            }
        };


        StreamlineIntegrationAlgorithm( InitData& data )
            : DataAlgorithm( data )
        {
        }

        virtual void execute( const Algorithm::Options& options, const volatile bool& /*abortFlag*/ ) override
        {
            std::shared_ptr<
                Field<
                    D,
                    Tensor<double, D>
                > const
            > field = options.get<
                Field<
                    D,
                    Tensor<double, D>
                >
            >("Field");

            std::shared_ptr<
                Function<
                    Tensor<double, D>
                > const
            > function = options.get<
                Function<
                    Tensor<double, D>
                >
            >("Field");

            Point<D> start = options.get<
                Point<D>
            >("start");

            if(!field) {
                throw std::runtime_error(
                    "uninitialized input field"
                );
            }

            std::shared_ptr<
                const Grid<D>
            > grid = std::dynamic_pointer_cast<Grid<D> const>(
                function->domain()
            );
            if(!grid) {
                throw std::logic_error("incompatible grid");
            }

            Scalar const step_size(options.get<double>("step size"));
            Scalar const halving_threshold(
                options.get<double>("halving threshold")
            );
            Scalar const doubling_threshold(
                options.get<double>("doubling threshold")
            );

            std::shared_ptr<Integrator> tracing;

            if ("Runge-Kutta" == options.get<std::string>("integrator")) {
                tracing.reset(new RungeKuttaIntegrator(field, step_size));
            } else {
                tracing.reset(
                    new AdaptiveEulerIntegrator(
                        field,
                        step_size,
                        halving_threshold,
                        doubling_threshold
                    )
                );
            }

            std::shared_ptr<
                typename lineset_chooser<D>::type
            > streamlines(new typename lineset_chooser<D>::type);

            Tensor<double, D> const slide = options.get<
                Tensor<double, D>
            >("slide delta");

            int const count = options.get<int>("step count");
            int const steps = options.get<int>("slide steps");

            for (int i = 0; steps > i; i++, start += slide) {
                std::vector<
                    Point<D>
                > line_points = tracing->line_points(start, count);

                std::vector<size_t> line(line_points.size());

                typename std::vector<
                    Point<D>
                >::const_iterator reading = line_points.begin();

                typename std::vector<size_t>::iterator writing = line.begin();

                for (; line_points.end() != reading; reading++, writing++) {
                    *writing = streamlines->addPoint(*reading);
                }

                streamlines->addLine(line);
            }

            setResult(
                "streamlines",
                lineset_chooser<D>::do_export(streamlines)
            );
        }
    };

    AlgorithmRegister<
        StreamlineIntegrationAlgorithm<2>
    > integrator2d("Practical/Task4_2D", "Compute streamlines in 2D");

    AlgorithmRegister<
        StreamlineIntegrationAlgorithm<3>
    > integrator3d("Practical/Task4_3D", "Compute streamlines in 3D");
}
