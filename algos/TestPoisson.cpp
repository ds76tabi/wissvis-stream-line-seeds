#include <fantom/algorithm.hpp>
#include <fantom/datastructures/interfaces/Field.hpp>
#include <math.h>
#include <stdexcept>
#include <vector>

#include <fantom/graphics.hpp>
#include <fantom/register.hpp>
#include <fantom/dataset.hpp>
#include <fantom-plugins/utils/Graphics/HelperFunctions.hpp>
#include <fantom-plugins/utils/Graphics/ObjectRenderer.hpp>
#include <fantom/datastructures/domains/LineSet.hpp>


#include "VoronoiDiagramGenerator.h"
#include "PoissonFill.hpp"
#include "Datastructures.hpp"


using namespace fantom;
namespace {
    class MarkerAlgorithm : public VisAlgorithm {
    public:
        struct VisOutputs : public VisAlgorithm::VisOutputs {
            VisOutputs(fantom::VisOutputs::Control &control)
                    : VisAlgorithm::VisOutputs(control) {
                addGraphics("Marker");
                addGraphics("Streamlines");
                addGraphics("PoissonPoints");
            }
        };

        struct Options : public VisAlgorithm::Options {
            Options(fantom::Options::Control &control)
                    : VisAlgorithm::Options(control) {

                add<Color>("ColorVoronoiPoints", "The color of the graphics.", Color(0.0, 0.0, 1.0));
                add<Color>("ColorPoissonPoints", "The color of the graphics.", Color(1.0, 0.0, 0.0));

            }
        };


        MarkerAlgorithm(InitData &data)
                : VisAlgorithm(data) {
        }

        virtual void execute(const Algorithm::Options &options, const volatile bool &abortFlag) override {

            Color color_voronoi = options.get<Color>("ColorVoronoiPoints");
            Color color_poisson = options.get<Color>("ColorPoissonPoints");
            float xValues[4] = {-22, -17, 4,22};
	        float yValues[4] = {-9, 31,13,-5};

            long count = 4;

            voronoi::VoronoiDiagramGenerator vdg;
            vdg.generateVoronoi(xValues,yValues,count, -100,100,-100,100,3);

            vdg.resetIterator();

            float x1,y1,x2,y2;

            printf("\n-------------------------------\n");
            std::vector<Point3> punkte;


            Point3 a = {-22, -9, 0};
            Point3 b = {-17, 31, 0};
            Point3 c = {4, 13, 0};
            Point3 d = {22, -5, 0};

            punkte.push_back(a);
            punkte.push_back(b);
            punkte.push_back(c);
            punkte.push_back(d);

            std::vector<Point3> points;

            while(vdg.getNext(x1,y1,x2,y2))
            {
                printf("GOT Line (%f,%f)->(%f,%f)\n",x1,y1,x2, y2);
                Point3 start = {x1, y1, 0};
                Point3 end = {x2, y2, 0};
                points.push_back(start);
                points.push_back(end);

            }


            std::vector <fantom::VectorF<3>> StreamLines;
            for(size_t i = 0; i< points.size(); i++){

                StreamLines.push_back(fantom::VectorF<3>(points[i]));
            }

            auto const &system = graphics::GraphicsSystem::instance();

            
            
            // render the stuff

            auto performanceObjectRenderer = std::make_shared< graphics::ObjectRenderer >( system );

            performanceObjectRenderer->reserve( graphics::ObjectRenderer::ObjectType::SPHERE, (int)count );
            for (size_t j = 0; j < punkte.size() ; ++j) {
                printf("%f",punkte[j]);
                performanceObjectRenderer->addSphere( punkte[j], 0.55, color_voronoi );

            }
            setGraphics( "Marker", performanceObjectRenderer->commit() );

            std::string resourcePath = PluginRegistrationService::getInstance().getResourcePath("utils/Graphics");


            auto bs = graphics::computeBoundingSphere(StreamLines);
            std::shared_ptr <graphics::Drawable> geometryDrawable
                    = system.makePrimitive(graphics::PrimitiveConfig{graphics::RenderPrimitives::LINES}
                                                   .vertexBuffer("in_vertex", system.makeBuffer(StreamLines))
                                                   .uniform("u_lineWidth", 1.0f)
                                                   .uniform("u_color", color_voronoi)
                                                   .boundingSphere(bs),
                                           system.makeProgramFromFiles(
                                                   resourcePath + "shader/line/noShading/singleColor/vertex.glsl",
                                                   resourcePath + "shader/line/noShading/singleColor/fragment.glsl",
                                                   resourcePath +
                                                   "shader/line/noShading/singleColor/geometry.glsl"));

            setGraphics("Streamlines", geometryDrawable);

        }
    };

    AlgorithmRegister <MarkerAlgorithm> dummy("Hauptaufgabe/Voronoi", "voronoidrawer.");
}